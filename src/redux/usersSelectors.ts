import { AppStateType } from './reduxStore';
import { createSelector } from "reselect";

const getUsersSelector = (state: AppStateType) => {
    return state.usersPage.usersData;
}

export const getUsers = createSelector(getUsersSelector, (usersData) => {
    return usersData.filter((user:any) => true);
});
// export const getUsers = createSelector(getUsersSelector, (usersData) => {
//     return usersData.filter((user:any) => user.photos.small != null);
// });

export const getPageSize = (state: AppStateType) => {
    return state.usersPage.pageSize;
}

export const getTotalUsersCount = (state: AppStateType) => {
    return state.usersPage.totalUsersCount;
}

export const getCurrentPage = (state: AppStateType) => {
    return state.usersPage.currentPage;
}
export const getIsFetching = (state: AppStateType) => {
    return state.usersPage.isFetching;
}
export const getFollowingInProgress = (state: AppStateType) => {
    return state.usersPage.followingInProgress;
}
export const getUsersFilter = (state: AppStateType) => {
    return state.usersPage.filter
}