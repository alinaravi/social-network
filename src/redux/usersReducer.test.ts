
import usersReducer, { actions, InitialStateType } from './usersReducer';

let state: InitialStateType;

beforeEach(() => {
    state = {
        usersData: [
            {
                id: 0, name: 'Dimych 0', followed: false,
                photos: {small: null, large: null}, status: 'status 0'
            },
            {
                id: 1, name: 'Dimych 1', followed: false,
                photos: {small: null, large: null}, status: 'status 1'
            },
            {
                id: 2, name: 'Dimych 2', followed: true,
                photos: {small: null, large: null}, status: 'status 2'
            },
            {
                id: 3, name: 'Dimych 3', followed: true,
                photos: {small: null, large: null}, status: 'status 3'
            },
        ],
        pageSize: 10,
        totalUsersCount: 0,
        currentPage: 1,
        isFetching: false,
        followingInProgress: [],
        filter: {
            term: '',
            friend: null as null | boolean 
        }
    }
})

test('follow success', () => {
    const newState = usersReducer(state, actions.followSuccess(1))

    expect(newState.usersData[0].followed).toBeFalsy();
    expect(newState.usersData[2].followed).toBeTruthy();
})

test('unfollow success', () => {
    const newState = usersReducer(state, actions.unfollowSuccess(3))

    expect(newState.usersData[3].followed).toBeTruthy();
    expect(newState.usersData[1].followed).toBeFalsy();
})