import { AppStateType } from './reduxStore';
import { createSelector } from "reselect";

export const profileSelector = (state: AppStateType) => {
    return state.profilePage.profile;
}

// export const getUsers = createSelector(getUsersSelector, (usersData) => {
//     return usersData.filter((user:any) => true);
// });

export const statusSelector = (state: AppStateType) => {
    return state.profilePage.status;
}

// export const getTotalUsersCount = (state: AppStateType) => {
//     return state.usersPage.totalUsersCount;
// }

// export const getCurrentPage = (state: AppStateType) => {
//     return state.usersPage.currentPage;
// }
// export const getIsFetching = (state: AppStateType) => {
//     return state.usersPage.isFetching;
// }
// export const getFollowingInProgress = (state: AppStateType) => {
//     return state.usersPage.followingInProgress;
// }
// export const getUsersFilter = (state: AppStateType) => {
//     return state.usersPage.filter
// }