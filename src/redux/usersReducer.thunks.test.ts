import { ApiResponseType, ResultCodesEnum } from './../api/api';
import { follow, actions, unFollow } from './usersReducer';
import { usersAPI } from './../api/usersApi';

jest.mock('./../api/usersApi')
const usersAPIMock = usersAPI as jest.Mocked<typeof usersAPI>

const dispatchMock = jest.fn()
const getStateMock = jest.fn()

beforeEach(() => {
  dispatchMock.mockClear()
  getStateMock.mockClear()
  usersAPIMock.postFollwed.mockClear()
  usersAPIMock.deleteUnFollwed.mockClear()
})

const result: ApiResponseType = {
  resultCode: ResultCodesEnum.Success,
  messages: [],
  data: {}
}

usersAPIMock.postFollwed.mockReturnValue(Promise.resolve(result))
usersAPIMock.deleteUnFollwed.mockReturnValue(Promise.resolve(result))


test('success follow thunk', async () => {
  const thunk = follow(1)
  await thunk(dispatchMock, getStateMock, {})

  expect(dispatchMock).toBeCalledTimes(3) 
  expect(dispatchMock).toHaveBeenNthCalledWith(1, actions.togglefollowingInProgress(true, 1)) 
  expect(dispatchMock).toHaveBeenNthCalledWith(2, actions.followSuccess(1)) 
  expect(dispatchMock).toHaveBeenNthCalledWith(3, actions.togglefollowingInProgress(false, 1)) 
})


test('success unfollow thunk', async () => {
  const thunk = unFollow(1)
  await thunk(dispatchMock, getStateMock, {})

  expect(dispatchMock).toBeCalledTimes(3) 
  expect(dispatchMock).toHaveBeenNthCalledWith(1, actions.togglefollowingInProgress(true, 1)) 
  expect(dispatchMock).toHaveBeenNthCalledWith(2, actions.unfollowSuccess(1)) 
  expect(dispatchMock).toHaveBeenNthCalledWith(3, actions.togglefollowingInProgress(false, 1)) 
})