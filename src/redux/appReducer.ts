import { authThunkCreator } from "./authReducer";
import { InferActionsTypes } from "./reduxStore";

const SET_INITIALIZED_SUCCESS = 'SN/APP/SET_INITIALIZED_SUCCESS'

let initialState = {
    initialized: false
}
export type InitialStateType = typeof initialState
type ActionsType = InferActionsTypes<typeof actions>

// type ActionType = InterActionsTypes<typeof action>

const appReducer = (state = initialState, action: ActionsType): InitialStateType => {
    switch (action.type) {
        case SET_INITIALIZED_SUCCESS:
            return {
                ...state,
                initialized: false,    
        }    

    default:
        return state;
    }
}

export const actions = {
    initializedSuccess: () => ({type: SET_INITIALIZED_SUCCESS })
}



export const initializeApp = () => (dispatch: any) => {
    let promise = dispatch(authThunkCreator());
    Promise.all([promise])
        .then(() => {
        dispatch(actions.initializedSuccess());
    })
}

export default appReducer;