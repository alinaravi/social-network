import { FormAction } from 'redux-form/lib/actions';
import { BaseThunkType, InferActionsTypes } from './reduxStore';
import { act } from "react-dom/test-utils";


const ADD_MESSAGE = 'SN/DIALOG/ADD_MESSAGE'
const START_CHATTING = 'SN/DIALOG/START_CHATTING'
const GET_DIALOGS = 'SN/DIALOG/GET_DIALOGS'
// const GET_MESSAGES = 'SN/DIALOG/GET_MESSAGES'


export type DialogType = {
    id: number,
    name: string
}
type MessageType = {
    id: number,
    message: string
}
let initialState = {
    messagesData: [{
        message: 'Hi',
        id: 1
    },
    {
        message: 'Yo',
        id: 2
    },
    {
        message: 'How are...',
        id: 3
    }
    ] as Array<MessageType>,

    dialogsData: [{
        name: 'Dima',
        id: 1
    },
    {
        name: 'Tim',
        id: 2
    },
    {
        name: 'Lara',
        id: 3
    }
    ] as Array<DialogType>,

}




const dialogReducer = (state = initialState, action: ActionsTypes): InitialStateType => {
    switch (action.type) {
        case ADD_MESSAGE:
            let body = action.messageBody;
            return {
                ...state,
                messagesData: [...state.messagesData, { id: 5, message: body }]
            };
       
     
        default:
            return state;

    }
}

export const actions = {
    sendMessage: (messageBody: string) => ({
            type: ADD_MESSAGE,
            messageBody
    } as const),

}

//thunk



export default dialogReducer

export type InitialStateType = typeof initialState
type ActionsTypes = InferActionsTypes<typeof actions>
type ThunkType = BaseThunkType<ActionsTypes | FormAction>