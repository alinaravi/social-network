import { stopSubmit } from 'redux-form';
import { getCaptchaUrl } from './../api/securityApi';
import { getLogin, authLogin, logOut } from './../api/authApi';
import { ResultCodeForCaptchaEnum, ResultCodesEnum } from './../api/api';
import { AppStateType, BaseThunkType, InferActionsTypes } from './reduxStore';
import { ThunkAction } from 'redux-thunk';

import {FormAction} from 'redux-form/lib/actions';
import { Action } from 'redux';

//geben den Action eine einzigartige name
const SET_USER_DATA = 'SN/AUTH/SET_USER_DATA'
const GET_CAPTCHA_URL_SUCCESS = 'SN/AUTH/GET_CAPTCHA_URL_SUCCESS'


let initialState = {
    userId: null as (number | null),
    email: null as string | null,
    login: null as string | null,
    isAuth: false as boolean,
    messages: '' as string | null, 
    captchaUrl: null as string | null//if null, then captcha is not required
}

const authReducer = (state = initialState, action: ActionsTypes): InitialStateType => {
    switch (action.type) {
        case SET_USER_DATA:
        case GET_CAPTCHA_URL_SUCCESS:
            return {
                ...state,
                ...action.payload,  
        }   

    default:
        return state;
    }
}
export const actions = {
   setUserData: (userId: number | null, email: string| null, login: string| null, isAuth: boolean) => ({
        type: SET_USER_DATA,
        payload: {
            userId, email, login, isAuth
        } 
    } as const),
    getCaptchaUrlSuccess: (captchaUrl: string) => ({
            type: GET_CAPTCHA_URL_SUCCESS,
            payload: {
                captchaUrl
            }
        } as const)
}

//thunk - macht asynchrone Operationen, sie dispatch einfache Actionen; mehrere Operation werden durhc thunk ersetzen
//redux führ die Func durch


export const authThunkCreator = (): ThunkType => async (dispatch) => {
    let meData = await getLogin();
            if(meData.resultCode === ResultCodesEnum.Success) {
                let {id, email, login} = meData.data;
               dispatch (actions.setUserData(id, email, login, true));
             } 
            //else {
            //      let messages = response.data.messages[0]
            //     console.log(messages)
            //     dispatch (setUserData(messages));
            // } 
 }

 export const getCaptchaUrlThunk = (): ThunkType => async (dispatch) => {
    const captchaData = await getCaptchaUrl();
    const captchaURL = captchaData.url;
    dispatch (actions.getCaptchaUrlSuccess(captchaURL));
 }

 export const loginThunkCreator = (email: string, password: string, rememberMe:boolean, captcha: string): ThunkType => async (dispatch) => {
    let loginData = await authLogin(email, password, rememberMe, captcha);
        if(loginData.resultCode === ResultCodesEnum.Success) {
            dispatch(authThunkCreator())
        }  else {
            if(loginData.resultCode === ResultCodeForCaptchaEnum.CaptchaIsRequired) {
                dispatch(getCaptchaUrlThunk())
            }
        //wir wollen aufhören mit Submit
        //wir stopem form: login (in Login Component)
            let message = loginData.messages.length > 0 ? loginData.messages[0] : 'Some error'
            dispatch(stopSubmit('login', {_error: message}))
        }
}

export const logoutThunk = (): ThunkType => async (dispatch) => {
    let logoutData = await logOut();
        if(logoutData.resultCode === 0) {
            dispatch (actions.setUserData(null, null, null, false));
        } 
}
 


export default authReducer;

export type InitialStateType = typeof initialState
type ActionsTypes = InferActionsTypes<typeof actions>
type ThunkType = BaseThunkType<ActionsTypes | FormAction>