import { usersAPI } from './../api/usersApi';
import { ResultCodesEnum, ApiResponseType } from './../api/api';
import { BaseThunkType, InferActionsTypes } from './reduxStore';
import { UsersDataType } from "../types/types";
import {
    updateObjectInArray
} from '../utils/objectHelpers';
import { Dispatch } from 'react';



const FOLLOW = 'SN/USERS/FOLLOW'
const UNFOLLOW = 'SN/USERS/UNFOLLOW'
const SET_USERS = 'SN/USERS/SET_USERS'
const SET_CURRENT_PAGE = 'SN/USERS/SET_CURRENT_PAGE'
const SET_TOTAL_USERS_COUNT = 'SN/USERS/SET_TOTAL_USERS_COUNT'
const TOGGLE_IS_FETCHING = 'SN/USERS/TOGGLE_IS_FETCHING'
const TOGGLE_IS_FOLLOWING_PROGRESS = 'SN/USERS/TOGGLE_IS_FOLLOWING_PROGRESS'
const SET_FILTER = 'SN/USERS/SET_FILTER'

let initialState = {
    usersData: [] as Array<UsersDataType>,
    pageSize: 9,
    totalUsersCount: 0,
    currentPage: 1,
    isFetching: true,
    followingInProgress: [] as Array<number>, //arrayof usersId
    filter: {
        term: '',
        friend: null as null | boolean 
    }
}


const usersReducer = (state = initialState, action: ActionsTypes): InitialStateType => {
    switch (action.type) {
        //Button follow
        case FOLLOW:
            return {
                //kopie
                ...state,
                usersData: updateObjectInArray(state.usersData, action.userId, 'id', {followed: true})
            }

        //Button unfollow
        case UNFOLLOW:
            return {
                ...state,
                usersData: updateObjectInArray(state.usersData, action.userId, 'id', {followed: false})
            }

        case SET_USERS:
            return {
                ...state,
                usersData: [...action.usersData]
            }

        //change Page
        case SET_CURRENT_PAGE:
            return {
                ...state,
                currentPage: action.currentPage
            }

        //total page, but to match
        case SET_TOTAL_USERS_COUNT:
            return {
                ...state,
                totalUsersCount: action.count
            }

        case TOGGLE_IS_FETCHING:
            return {
                ...state,
                isFetching: action.isFetching

            }

        case TOGGLE_IS_FOLLOWING_PROGRESS:
            return {
                ...state,
                followingInProgress: action.isFetching ?
                    [...state.followingInProgress, action.userId] :
                    state.followingInProgress.filter(id => id !== action.userId)

            }
        case SET_FILTER:
                return {
                    ...state,
                    filter: action.payload
                }

        default:
            return state;
    }
}

//gesamt type action

export const actions = {
    //diese functionen geben action zurück
    followSuccess: (userId: number) => ({
        type: FOLLOW,
        userId
    } as const),

    unfollowSuccess: (userId: number) => ({
        type: UNFOLLOW,
        userId
    } as const),
    setUsers: (usersData: Array<UsersDataType>) => ({
        type: SET_USERS,
        usersData
    } as const),

    setCurrentPage: (currentPage: number) => ({
        type: SET_CURRENT_PAGE,
        currentPage
    } as const),

    //die gesammte Usersanzahl/Pageanzahl
    setTotalUsersCount: (totalUsersCount: number) => ({
        type: SET_TOTAL_USERS_COUNT,
        count: totalUsersCount
    } as const),

    toggleIsFetching: (isFetching: boolean) => ({
        type: TOGGLE_IS_FETCHING,
        isFetching
    } as const),
    togglefollowingInProgress: (isFetching: boolean, userId: number) => ({
        type: TOGGLE_IS_FOLLOWING_PROGRESS,
        isFetching,
        userId
    } as const),
    setFilter: (filter: FilterType) => ({
        type: SET_FILTER,
        payload: filter
    } as const)
}

//Thunk

export const getUsersThunkCreator = (currentPage: number, pageSize: number, filter: FilterType): ThunkType => {
    return async (dispatch, getState) => {
        dispatch(actions.toggleIsFetching(true))
        dispatch(actions.setCurrentPage(currentPage))
        dispatch(actions.setFilter(filter))

        //Detail wurden in anderen Ordner verschoben
        let data = await usersAPI.getUsers(currentPage, pageSize, filter.term, filter.friend)
        //response ist jetzt in api.js, wir nehmen nur data
        dispatch(actions.toggleIsFetching(false))
        dispatch(actions.setUsers(data.items))
        dispatch(actions.setTotalUsersCount(data.totalCount))
    }
}

const _followUnfollowFlow = async (dispatch: Dispatch<ActionsTypes>,
                                    userId: number,
                                    apiMethod: (userId: number) => Promise<ApiResponseType>,
                                    actionCreator: (userId: number) => ActionsTypes) => {
    dispatch(actions.togglefollowingInProgress(true, userId))
    let response = await apiMethod(userId)

    if (response.resultCode == ResultCodesEnum.Success) {
        dispatch(actionCreator(userId))
    }
    dispatch(actions.togglefollowingInProgress(false, userId))
}

export const followThunk = (userId: number): ThunkType => {
    return async (dispatch) => {
        await _followUnfollowFlow(dispatch, userId, usersAPI.postFollwed.bind(usersAPI), actions.followSuccess)
    }
}
// export const follow = (userId: number): ThunkType => {
//     return async (dispatch) => {
//         dispatch(actions.togglefollowingInProgress(true, userId))
//         const data = await postFollwed(userId)

//         if (data.resultCode === ResultCodesEnum.Success) {
//             dispatch(actions.followSuccess(userId))
//         }
//         dispatch(actions.togglefollowingInProgress(false, userId))
//     }
// }

export const unFollowThunk = (userId: number): ThunkType => {
    return async (dispatch) => {
        await _followUnfollowFlow(dispatch, userId, usersAPI.deleteUnFollwed.bind(usersAPI), actions.unfollowSuccess)
    }
}

// export const unFollow = (userId: number): ThunkType => {
//     return async (dispatch) => {
//         dispatch(actions.togglefollowingInProgress(true, userId))
//         const data = await usersAPI.deleteUnFollwed(userId)
//         if (data.resultCode === ResultCodesEnum.Success) {
//             dispatch(actions.unfollowSuccess(userId))
//         }
//         dispatch(actions.togglefollowingInProgress(false, userId))
//     }
// }

export type InitialStateType = typeof initialState
export type FilterType = typeof initialState.filter
type ActionsTypes = InferActionsTypes<typeof actions>
type ThunkType = BaseThunkType<ActionsTypes>

export default usersReducer;