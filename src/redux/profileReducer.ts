import { getProfile, getStatus, updateStatus, savePhotoAPI, saveProfileAPI } from './../api/profileApi';
import { ResultCodesEnum } from './../api/api';
import { FormAction } from 'redux-form/lib/actions';
import { AppStateType, BaseThunkType, InferActionsTypes } from './reduxStore';
import { ThunkAction } from 'redux-thunk';

import { stopSubmit } from "redux-form";
import { PhotosType, PostType, ProfileType } from "../types/types";

const ADD_POST = 'SN/PROFILE/ADD_POST'
const SET_USER_PROFILE = 'SN/PROFILE/SET_USER_PROFILE'
const SET_STATUS = 'SN/PROFILE/SET_STATUS'
const DELETE_POST = 'SN/PROFILE/DELETE_POST'
const SAVE_PHOTO_SUCCESS = 'SN/PROFILE/SAVE_PHOTO_SUCCESS'


let initialState = {
  // postsData: [ {id: 1, message: 'Hi, how are you?', likesCount: 12},
  // {id: 2, message: 'It\'s my first post', likesCount: 11},
  // {id: 3, message: 'Blabla', likesCount: 11},
  // {id: 4, message: 'Dada', likesCount: 11}
  // ] as Array<PostType>,
  postsData: [ {id: 1, message: 'Hi, how are you?', likesCount: 12},
  {id: 2, message: 'It\'s my first post', likesCount: 11},
  {id: 3, message: 'Blabla', likesCount: 11},
  {id: 4, message: 'Dada', likesCount: 11}
  ] as Array<PostType>,
 
  profile: null as ProfileType | null ,
  status: ''
 
}

export type InitialStateType = typeof initialState
type ActionsTypes = InferActionsTypes<typeof actions>
type ThunkType = BaseThunkType<ActionsTypes | FormAction>

//reducer
//schauet auf action.type und generiert neu store
const profileReducer = (state = initialState, action: ActionsTypes): InitialStateType => {

  switch (action.type) {
    case ADD_POST: 
      let newPost = {
        id: 5,
        message: action.postBody,
        likesCount: 0
      }
      return {
        ...state,
        postsData: [...state.postsData, newPost],
      
      };
  
  
     case SET_USER_PROFILE:
        return {
          ...state,
          profile: action.profile
        };
    case SET_STATUS:
          return {
            ...state,
            status: action.status
          };
    case DELETE_POST:
          return {
            ...state,
            postsData: state.postsData.filter(p => p.id != action.postId)
          };
    case SAVE_PHOTO_SUCCESS:
          return {
            ...state,
            profile: {...state.profile, photos: action.photos} as ProfileType
            };

    default: {
      return state;
    }
  }
}

//acion creator - gibt action zurück
export const actions = {
  addPostActionCreator: (postBody: string) => ({
    type: ADD_POST,
    postBody
} as const),
setUserProfile: (profile: ProfileType) =>({
    type: SET_USER_PROFILE,
    profile
} as const),
setStatus: (status: string) => ({
    type: SET_STATUS,
    status
} as const),
deletePost: (postId: number) => ({
    type: DELETE_POST,
    postId
}as const),
savePhotoSuccess: (photos: PhotosType) => ({
    type: SAVE_PHOTO_SUCCESS,
    photos
}as const)
}

//thunk

export const profileThunkCreator = (userId: number): ThunkType => async (dispatch) => {
    const data = await getProfile(userId);
      dispatch(actions.setUserProfile(data)) 
}

export const getStatusThunk = (userId: number): ThunkType => async (dispatch) => {
  const data = await getStatus(userId)
      dispatch(actions.setStatus(data))    
} 

export const updateStatusThunk = (status: string): ThunkType => async (dispatch) => {
  try {
  const data = await updateStatus(status)
    if(data.resultCode === ResultCodesEnum.Success){
        dispatch(actions.setStatus(status)) 
    }
  } catch (error){
    console.log('Error')
    }
} 

export const savePhoto = (file: File): ThunkType => async (dispatch) => {
  let data = await savePhotoAPI(file)
    if(data.resultCode === ResultCodesEnum.Success){
        dispatch(actions.savePhotoSuccess(data.data.photos)) 
    } 
} 

export const saveProfile= (profile: ProfileType): ThunkType => async (dispatch, getState) => {
 const userId = getState().auth.userId
  const data = await saveProfileAPI(profile)

    if(data.resultCode === ResultCodesEnum.Success){
      if(userId != null) {
         dispatch(profileThunkCreator(userId)) 
      } else {
        throw new Error('userId can not be null')
      }
      
    } else {
      dispatch(stopSubmit('editProfile', {_error: data.messages[0]}))
     return Promise.reject(data.messages[0]);
    }
} 


export default profileReducer;