import dialogReducer from "./dialogReducer";

import profileReducer from "./profileReducer";
import usersReducer from "./usersReducer";
import authReducer from "./authReducer";
import appReducer from './appReducer'
import thunkMiddleware, { ThunkAction } from 'redux-thunk'
import { reducer as formReducer } from 'redux-form'
import { Action } from "redux";

const { createStore, combineReducers, applyMiddleware, compose } = require("redux");

//für jeder Ast ist ein reducer verantwortlich
let rootReducer = combineReducers({
    profilePage: profileReducer,
    dialogPage: dialogReducer,

    usersPage: usersReducer,
    auth: authReducer,
    app: appReducer,
    form: formReducer,
})

type RootReducerType = typeof rootReducer 
export type AppStateType = ReturnType<RootReducerType>

type InterValueTypes<T> = T extends { [key: string]: infer U } ? U : never
export type InferActionsTypes<T extends { [key: string]: (...args: any[]) => any}> = ReturnType<InterValueTypes<T>>

export type BaseThunkType<A extends Action, R = Promise<void>> = ThunkAction<R, AppStateType, unknown, A>
// @ts-ignore
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunkMiddleware)));

//createStore erstellt drei Methoden innendrin und ist von reducer-bib
// let store = createStore(rootReducer, applyMiddleware(thunkMiddleware))
// @ts-ignore
window.__store__ = store

export default store;