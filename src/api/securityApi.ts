import { instance } from "./api";

type CaptchaType = {
    url: string
}
export const getCaptchaUrl = () => {
    return instance.get<CaptchaType>(`security/get-captcha-url`)
    .then(response => {
        return response.data;
    });
}