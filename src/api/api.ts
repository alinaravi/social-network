import { PhotosType, ProfileType, UsersDataType } from './../types/types';
import axios from 'axios';

export const instance = axios.create({
    withCredentials: true, 
    baseURL: 'https://social-network.samuraijs.com/api/1.0/',
    headers: {
        'API-KEY': 'e72db4f8-3ae0-4df8-8b9b-333c2fe1d2b3'
        //'API-KEY': 'fefb101d-f580-49aa-841c-316f4dee99de'
    }
});

export type GetItemsType = {
    items: Array<UsersDataType>
    totalCount: number
    error: string | null
}

export enum ResultCodesEnum {
    Success = 0,
    Error = 1
}
export enum ResultCodeForCaptchaEnum {
    CaptchaIsRequired = 10
}

export type ApiResponseType<D = {}, RC = ResultCodesEnum> = {
    data: D
    resultCode: RC
    messages: Array<string>
}






