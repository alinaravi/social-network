import { PhotosType, ProfileType } from "../types/types"
import { instance, ApiResponseType } from "./api"


export const getProfile = (userId: number) => {
    return instance.get<ProfileType>(`profile/${userId}`)
    .then(response => {
        return response.data;
    })
}

//URI_userId; Response_ string
export const getStatus = (userId: number) => {
    return instance.get<string>(`profile/status/${userId}`)
    .then(response => {
        return response.data;
    })
}

export const updateStatus = (status: string) => {
    return instance.put<ApiResponseType>(`profile/status`, {status})
    .then(response => {
        return response.data;
    })
}
type SavePhotoResponseDataType = {
    photos: PhotosType
}

export const savePhotoAPI = (photoFile: File) => {
    let formData = new FormData();
    formData.append('image', photoFile)
    return instance.put<ApiResponseType<SavePhotoResponseDataType>>(`profile/photo`, formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    }).then(response => {
        return response.data;
    });
}

export const saveProfileAPI = (profile: ProfileType) => {
    return instance.put<ApiResponseType>(`profile`, profile)
    .then(response => {
        return response.data;
    });
}