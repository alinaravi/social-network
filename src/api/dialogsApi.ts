import { DialogType } from './../redux/dialogReducer';
import { instance, ApiResponseType } from "./api"


export const putDialogs = (userId: number) => {
    return instance.put<ApiResponseType>(`dialogs/${userId}`)
    .then(response => {
        console.log(response.data)
        return response.data;
    })
}
// message: string
export const getDialogs = (dialogs: DialogType) => {
    return instance.get(`dialogs`)
    .then(response => {
        console.log(response.data)
        return response.data;
    })
}
// []
export const getMessages = (userId: number, page = 1, count = 10) => {
    return instance.get(`dialogs/${userId}/messages`)
    .then(response => {
        console.log(response.data)
        return response.data;
    })
}
// {
//     "items": [],
//     "totalCount": 0,
//     "error": null
//   }

export const postMessages = (userId: number) => {
    return instance.post<string>(`dialogs/${userId}/messages`)
    .then(response => {
        console.log(response.data)
        return response.data;
    })
}

export const viewedMessages = (messageId: number) => {
    return instance.get(`dialogs/messages/${messageId}/viewed`)
    .then(response => {
        console.log(response.data)
        return response.data;
    })
}

export const viewedSpanMessages = (messageId: number) => {
    return instance.post(`dialogs/messages/${messageId}/spam`)
    .then(response => {
        console.log(response.data)
        return response.data;
    })
}

export const deleteMessages = (messageId: number) => {
    return instance.delete(`dialogs/messages/${messageId}`)
    .then(response => {
        console.log(response.data)
        return response.data;
    })
}



export const restoreMessages = (messageId: number) => {
    return instance.put(`dialogs/messages/${messageId}/restore`)
    .then(response => {
        console.log(response.data)
        return response.data;
    })
}

export const newestMessages = (userId: number, date: string) => {
    return instance.delete(`dialogs/${userId}/messages/new?newerThen=${date}`)
    .then(response => {
        console.log(response.data)
        return response.data;
    })
}

export const countMessages = () => {
    return instance.get(`dialogs/messages/new/count`)
    .then(response => {
        console.log(response.data)
        return response.data;
    })
}