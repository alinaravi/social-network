
import { instance, ApiResponseType, ResultCodeForCaptchaEnum, ResultCodesEnum} from "./api";


type MeResponseDataType = {
    id: number, email: string, login: string 
}

type LoginResponseType = {
    userId: number
}

export const authLogin = (email: string, password: string, rememberMe: boolean = false, captcha: null | string = null) => {
    return instance.post<ApiResponseType<LoginResponseType, ResultCodesEnum | ResultCodeForCaptchaEnum>>(`auth/login`, {email, password, rememberMe, captcha}) 
    .then(response => {
        return response.data;
    });
}


export const getLogin = () => {
    return instance.get<ApiResponseType<MeResponseDataType>>(`auth/me`)
        .then(response => {
            return response.data;
        });
}

export const logOut = () => {
    return instance.delete(`auth/login`)
    .then(response => {
        return response.data;
    });
}