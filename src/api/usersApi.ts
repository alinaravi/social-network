
import { GetItemsType, instance, ApiResponseType } from './api';


export const usersAPI = {
    getUsers(currentPage = 1, pageSize = 9, term: string = '', friend: null | boolean = null) {
        return instance.get<GetItemsType>(`users?page=${currentPage}&count=${pageSize}&term=${term}` + (friend === null ? '' : `&friend=${friend}`))
            .then(response => {
                // console.log(response.data)
                return response.data;
            });
    },

    deleteUnFollwed(userId: number) {
        return instance.delete(`follow/${userId}`)
        .then(response => response.data) as Promise<ApiResponseType>
    },

    postFollwed(userId: number) {
        return instance.post<ApiResponseType>(`follow/${userId}`)
            .then(response => {
                return response.data;
            })
    }
}
