import * as serviceWorker from './serviceWorker';
import React from 'react';
import ReactDOM from 'react-dom';

import store from './redux/reduxStore'
import './index.css';
import SamuraiJSApp from "./App";

import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';


import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';



//render _ otrisivat 

ReactDOM.render(<SamuraiJSApp/>, document.getElementById('root'));

serviceWorker.unregister();