import React from 'react';
import { useSelector } from 'react-redux';
import Preloader from '../common/Preloader/Preloader';
import { getIsFetching } from '../../redux/usersSelectors';
import { Users } from './Users';



type PropsType = {
    pageTitle: string
}

export const UsersPage: React.FC<PropsType> = (props) => {

    const isFetching = useSelector(getIsFetching)

    return <>
    <div className="mainUsers">
    <h3>{props.pageTitle}</h3> 
    {isFetching ? <Preloader /> : null}
        <Users />
       </div>
    </>
}



