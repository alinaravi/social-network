import { Form, Formik, Field, ErrorMessage } from 'formik';
import React from 'react'
import { useSelector } from 'react-redux';
import { FilterType } from '../../redux/usersReducer';
import { getUsersFilter } from '../../redux/usersSelectors';
import { FaSistrix } from "react-icons/fa";

const usersSearchFormValidate = (values: any) => {
    const errors = {};
    return errors;
}
type FriendFormType = 'true' | 'false'| 'null'
type FormType = {
    term: string
    friend: FriendFormType
    // withFoto: string
}
type PropsType = {
    onFilterChanged: (filter: FilterType) => void
}
export const UsersSearchForm: React.FC<PropsType> = React.memo((props) => {

    const filter = useSelector(getUsersFilter)

    const submit = (values: FormType, { setSubmitting }: { setSubmitting: (isSubmitting: boolean) => void }) => {
       
        const filter: FilterType = {
            term: values.term,
            friend: values.friend === 'null' ? null : values.friend === 'true' ? true : false} 
           
        props.onFilterChanged(filter)
        setSubmitting(false)
    }

    return <div >
        {}
        <Formik
            enableReinitialize
            initialValues={{ term: filter.term, friend: String(filter.friend) as FriendFormType }}
            validate={usersSearchFormValidate}
            onSubmit={submit}
        >
            {({ isSubmitting }) => (
                <Form className="formInline">
                    <Field placeholder="Find your Friend" type="text" name="term"  className="form-control mr-sm-2"/>

                    <div>Followed / Unollowed:</div>
                    <Field name="friend" as="select"  className="form-control">
                        <option value="null">All</option>
                        <option value="true">Only followed</option>
                        <option value="false">Only unfollowed</option>
                    </Field>

                    <button type="submit" disabled={isSubmitting} className="btn btn-outline-primary my-2 my-sm-0">
                      <FaSistrix />
</button>
                </Form>
            )}
        </Formik>
    </div>
})

