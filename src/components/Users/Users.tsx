import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { FilterType, getUsersThunkCreator, unFollowThunk, followThunk } from '../../redux/usersReducer';
import { getCurrentPage, getFollowingInProgress, getPageSize, getTotalUsersCount, getUsers, getUsersFilter } from '../../redux/usersSelectors';
import { UsersDataType } from '../../types/types';
import Paginator from '../common/Paginator/Paginator';
import User from './User';

import * as queryString from 'querystring'
import { UsersSearchForm } from './UsersSearchForm';

type PropsType = {}
type QueryParamsType = { term?: string; page?: string; friend?: string }
//refactoring - make Hook
export const Users: React.FC<PropsType> = (props) => {
    //1. Hook useSelector, from UsersContainer (mapStateToProps)
    const usersData = useSelector(getUsers)
    const totalUsersCount = useSelector(getTotalUsersCount)
    const pageSize = useSelector(getPageSize)
    const currentPage = useSelector(getCurrentPage)
    const filter = useSelector(getUsersFilter)

    const followingInProgress = useSelector(getFollowingInProgress)

    const dispatch = useDispatch()
    const history = useHistory()

    useEffect(() => {
        document.title = "Users | SNet"
     }, []);
     
    useEffect(() => {
        const query: QueryParamsType = {}
        if (!!filter.term) query.term = filter.term
        if (filter.friend) query.friend = String(filter.friend)
        if (currentPage !==1) query.page = String(currentPage)
        history.push({
            pathname: '/users',
            search: `?term=${filter.term}&friend=${filter.friend}&page=${currentPage}`
        })

    }, [filter, currentPage])

    useEffect(() => {
        //mit substr ? weggemacht
        const parsed = queryString.parse(history.location.search.substr(1)) as QueryParamsType

        let actualPage = currentPage
        let actualFilter = filter
        if (!!parsed.page) actualPage = Number(parsed.page)
        if (!!parsed.term) actualFilter = { ...actualFilter, term: parsed.term as string }

        switch (parsed.friend) {
            case 'null':
                actualFilter = { ...actualFilter, friend: null }
                break
            case 'true':
                actualFilter = { ...actualFilter, friend: true }
               
                break
            case 'false':
                actualFilter = { ...actualFilter, friend: false }
                break
        }
        dispatch(getUsersThunkCreator(actualPage, pageSize, actualFilter))
    }, [])

    const onPageChanged = (pageNumber: number) => {
        dispatch(getUsersThunkCreator(pageNumber, pageSize, filter))
    }
    const onFilterChanged = (filter: FilterType) => {
        dispatch(getUsersThunkCreator(1, pageSize, filter))
    }

    // const follow = (userId: number) => {
    //     dispatch(followThunk(userId));
    // }
    // const unFollow = (userId: number) => {
    //     dispatch(unFollowThunk(userId));
    // }


    return <div >
       
        <div className="mainUsers">
        <UsersSearchForm
            onFilterChanged={onFilterChanged} />
        <Paginator
            currentPage={currentPage}
            onPageChanged={onPageChanged}
            totalItemsCount={totalUsersCount}
            pageSize={pageSize}
        /></div>
        <div className='users-wrapper'>
            {
                usersData.map((user: UsersDataType) => <User key={user.id}
                    user={user}
                    followingInProgress={followingInProgress}
                    // unFollow={unFollow}
                    // follow={follow} 
                    />)
            }
        </div>
    </div>
}
