import React from 'react'
import userPhoto from '../../asssets/images/user.jpg'
// import styles from './Users.module.css';
import { NavLink } from 'react-router-dom';
import { Pagination, Button } from 'react-bootstrap';
import * as axios from 'axios';
import Paginator from '../common/Paginator/Paginator';
import { PhotosType, UsersDataType } from '../../types/types';
import { useDispatch } from 'react-redux';
import { followThunk, unFollowThunk } from '../../redux/usersReducer';
import avatar from '../../asssets/images/cartoon.jpg' 
import './Users.scss'
type PropsType = {
    user: UsersDataType
    photos?: Array<PhotosType>
    followingInProgress: Array<number>
    // unFollow: (userId: number) => void
    // follow: (userId: number) => void
}
let User: React.FC<PropsType> = (props) => {
    const dispatch = useDispatch()
    const follow = (userId: number) => {
        dispatch(followThunk(userId));
    }
    const unFollow = (userId: number) => {
        dispatch(unFollowThunk(userId));
    }

    return (<>
        <div className="wrapper-user">
            <div className="image-wrapper">

                <img src={props.user.photos.small != null ? props.user.photos.small : avatar} />

                <div className="team-hover">
                    <div className="desk">
                        <NavLink to={'/profile/' + props.user.id}>
                            <h4>Click here to learn more about {props.user.name}</h4> </NavLink>
                    </div>
                    <div className="s-link">
                        <a href="#"><i className="fa fa-facebook"></i></a>
                        <a href="#"><i className="fa fa-twitter"></i></a>
                        <a href="#"><i className="fa fa-google-plus"></i></a>
                    </div>
                </div>
            </div>
            <div className='description-wrapper'>

                <div>
                    <div className="name">{props.user.name}</div>

                    <div className="description">{props.user.status || 'No Status'}</div>

                </div>
                {props.user.followed
                    ? <button disabled={props.followingInProgress.some(id => id === props.user.id)} onClick={() => {

                        unFollow(props.user.id)

                    }} className="btn btn-outline-primary">Unfollow</button>
                    : <button disabled={props.followingInProgress.some(id => id == props.user.id)} onClick={() => {
                        follow(props.user.id)

                    }} className="btn btn-primary btn-sm">Follow</button>}
            </div>

        </div></>)

}


export default User