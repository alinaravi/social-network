import React, { useRef } from 'react';
import avatar from '../../asssets/images/cartoon.jpg'

// import { NavLink  } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import { FaOdnoklassniki, FaHandsHelping, FaMusic, FaUsers, FaRunning, FaLanguage } from "react-icons/fa";
import { BsChatSquareQuote, BsNewspaper, BsPersonCheckFill } from "react-icons/bs";
import './Nav.scss'
import { useDispatch, useSelector } from 'react-redux';
import { isAuthSelector, loginSelector } from '../../redux/authSelectors';
import { logoutThunk } from '../../redux/authReducer'
import { AiFillSetting } from 'react-icons/ai';
import { ProfileType } from '../../types/types';
import { profileSelector } from '../../redux/profileSelectors';
import Preloader from '../common/Preloader/Preloader';
import { AppStateType } from '../../redux/reduxStore';
import { BiHelpCircle } from 'react-icons/bi';
import { GrLanguage } from 'react-icons/gr';
import i18n from '../../i18n'
import { withNamespaces } from 'react-i18next';
import Users from '../../asssets/images/teamwork.png'
export type MapPropsType = {
  // profile: ProfileType | null
  // isOwner: boolean
}


const NavbarComponent: React.FC<MapPropsType> = (props: any) => {
  const profile = useSelector(profileSelector)
  const isAuth = useSelector(isAuthSelector)
  const authUserId = useSelector((state: AppStateType) => state.auth.userId)
  const savedHandler = useRef();
  const dispatch = useDispatch()
  const logout = () => {
    dispatch(logoutThunk())
  }
  if (!profile) {
    return <Preloader />
  }

  //  const login = useSelector(loginSelector)
  const changeLanguage = (lng: any) => {
    i18n.changeLanguage(lng);
  }
  console.log(props.i18n.language)

  

  return (<>
    <nav className="navbar navbar-expand-lg navbar-light" style={{ backgroundColor: "#4d78eb" }}>

      <button type="button" className="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse colorIcon" id="navbarCollapse">

        <div className="navbar-nav mr-auto">
          <NavLink to='/profile' className="tags"><span className='react-icons'><FaOdnoklassniki /></span>{props.t('Profile')}</NavLink>
          <NavLink to='/dialogs' className="tags"><span className='react-icons'><BsChatSquareQuote /></span>{props.t('Message')}</NavLink >

          <NavLink to='/users' className="tags"><span className='react-icons'><FaUsers /></span>{props.t('Users')}</NavLink >

          <NavLink to='/news' className="tags"><span className='react-icons'><BsNewspaper /></span>{props.t('News')}</NavLink >

          <NavLink to='/music' className="tags"><span className='react-icons'><FaMusic /></span>{props.t('Music')}</NavLink >

          <NavLink to='/friends' className="tags"><span className='react-icons'><FaHandsHelping /></span>{props.t('Friends')}</NavLink >

        </div>


        <div className="form-inline my-2 my-lg-0">
          <div className='nav-item dropdown'>
            <div className="nav-link dropdown-toggle"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             <span className='react-icons'><GrLanguage /></span>{props.i18n.language}
            </div>
            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <div className="dropdown-item" onClick={() => changeLanguage('de')}>De</div>
              <div className="dropdown-item" onClick={() => changeLanguage('en')}>En</div>
            </div>
          </div>

          {isAuth ?
            <div className="nav-item dropdown">
              <a className="nav-link dropdown-toggle tags" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {authUserId === profile.userId &&
                <span>
                  <img src={profile.photos.large ? profile.photos.large : avatar} />
                  <span>{profile.fullName}</span>
                </span>
            }
          </a>
              <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                <NavLink className="dropdown-item tags" to="/friends"><span className='react-icons'><BiHelpCircle /></span>Hilfe</NavLink>
                <NavLink className="dropdown-item tags"  to='/settings'><span className='react-icons'><AiFillSetting /></span>{props.t('Settings')}</NavLink>
                <NavLink className="dropdown-item tags" to='/users' onClick={logout}  ><span className='react-icons'><FaRunning /></span>Log out</NavLink >
              </div>
            </div> :
            <NavLink to='/login' className="justify-content-end tags" ><span className='react-icons'><BsPersonCheckFill /></span>Login</NavLink >}

        </div>
      </div>
    </nav>
  </>
  )
}


export default withNamespaces()(NavbarComponent);