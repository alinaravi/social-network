import React from 'react';
import preloader from '../../../asssets/images/Spinner-1s-200px.svg'


let Preloader: React.FC = () => {
return <div>
 <img src={preloader} />
</div>
}

export default Preloader;