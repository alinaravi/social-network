import React from 'react'
import { FieldValidatorType } from '../../../utils/validator/validators'

import styles from './FormControls.module.css'
import {Field, WrappedFieldProps} from "redux-form"
import {WrappedFieldMetaProps} from 'redux-form/lib/Field'


type FormControlPropsType = {
    meta: WrappedFieldMetaProps
}

const FormControl: React.FC<FormControlPropsType> = ({meta: {touched, error}, children}) => {
    const hasError = touched && error;
    return (
        <div className={styles.formControl + " " + (hasError ? styles.error : "")}>
            <div>
                {children}
            </div>
            {hasError && <span>{error}</span>}
        </div>
    )
}



export const Textarea: React.FC<WrappedFieldProps> = (props) => {
    const {input, meta, children , ...restProps} = props;
    return (<FormControl {...props}><textarea {...restProps} {...input} /></FormControl>)
}

export const Input: React.FC<WrappedFieldProps>= (props) => {
    const {input, meta, children , ...restProps} = props;
 return(<FormControl {...props}><input {...restProps} {...input} /></FormControl>)

}


export function createField<FormKeysType extends string>(placeholder: string | undefined,
                            name: FormKeysType,
                            validators: Array<FieldValidatorType>,
                            component: React.FC<WrappedFieldProps>,
                            props = {}, text = "") {
    return <div>
        <Field placeholder={placeholder} name={name}
               validate={validators}
               component={component}
               {...props}
               className="form-control"
        /> {text}
    </div>
}
    
export type GetStringKeys<T> = Extract<keyof T, string>