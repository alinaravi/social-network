import React from 'react';
import { AiFillHome } from 'react-icons/ai';
import { NavLink } from 'react-router-dom';
import './NotFound.scss'
import { BiSupport } from "react-icons/bi";

export const NotFound: React.FC = (props) => {
    return (
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <div className="error-template">
                        <h1>
                            Oops!</h1>
                        <h2>
                            404 Not Found</h2>
                        <div className="error-details">
                            Sorry, an error has occured, Requested page not found!
                        </div>
                        <div className="error-actions">
                        <NavLink  to='/profile'  className="btn btn-primary btn-lg"><span className='react-icons'><AiFillHome /></span>
                        Take Me Home </NavLink>
                            <NavLink to="http://www.jquery2dotnet.com" className="btn btn-outline-secondary btn-lg"><span className='react-icons'> <BiSupport /></span> Contact Support </NavLink>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
