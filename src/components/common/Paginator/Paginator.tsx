import React, { useState } from 'react'
import styles from './Paginator.module.css';
import cn from 'classnames'
import { Button } from 'react-bootstrap';

type PropsType = {
    totalItemsCount: number
    pageSize: number
    currentPage?: number
    onPageChanged?: (pageNumber: number) => void
    portionSize?: number
}


//FC- functionalComponen; für functionale Componenten
let Paginator: React.FC<PropsType> = ({totalItemsCount, pageSize, currentPage = 1, onPageChanged = () => {}, portionSize = 9 }) => {  

    let pagesCount = Math.ceil(totalItemsCount / pageSize);

    let pages: Array<number> = [];

   
    for (let i = 1; i <= pagesCount; i++) {
            pages.push(i);
        }
    
    let portionCount = Math.ceil(pagesCount / portionSize);
    let [portionNumber, setPortionNumber] = useState<number>(1);

    let leftPortionPageNumber = (portionNumber - 1) * portionSize + 1;
    let rightPortionPageNumber = portionNumber * portionSize;
    

    return <div className='pagination'>
        {portionNumber > 1 &&
        <button onClick={ () => {setPortionNumber(portionNumber - 1)}} className="page-link">Prev</button>}

            {pages
            .filter(page => page >= leftPortionPageNumber && page <= rightPortionPageNumber)
            .map(page => {
                return <span className='page-item'
                key={page}
                    onClick={(e) => { onPageChanged(page) }}><span className='page-link'>{page}</span></span>
            })}
            {portionCount > portionNumber &&
            <button onClick={ () => {setPortionNumber(portionNumber + 1)}} className="page-link">Next</button>}
    </div>
}

export default Paginator