import React from 'react';
import { NewsType } from '../../../types/types';
import { ArticleItem } from './ArticleItem';


type PropsType = {
    articles: Array<NewsType>
}
const ArticleList: React.FC<PropsType> = (props) => {
    return (
        <div className="list-group list-group-flush" style={{ maxWidth: 900, margin: "0 auto" }}>
        {props.articles.map((article: any, index) => (
          <ArticleItem article={article} key={article.title + index} />
        ))}
     </div>
    );
  };
  
  export default ArticleList;