import React, { useEffect, useState } from 'react';

type PropsType = {
    searchForTopic: (topic: string) => void
}
const SearchBar: React.FC<PropsType> = (props) => {

    const [searchTopic, setSearchTopic] = useState('')

    const handleChange = (event: any) => {
        setSearchTopic(event.target.value);
      };
    
     const handleSubmit = (event: any) => {
        event.preventDefault();
        props.searchForTopic(searchTopic);
      };
    return (
        <div style={{ display: "flex", justifyContent: "center" }}>
            <form onSubmit={handleSubmit}>
            <div className="form-group">
                <input
                 placeholder="Search topic"
                 name="topic"
                 value={searchTopic}
                 onChange={handleChange} />
                 <button type="submit">Search</button>

            </div>

            </form>

        </div>
    )
}

export default SearchBar;