import React, { useEffect, useState } from 'react';
import { NewsType } from '../../../types/types';
import '../News.scss'

type PropsType = {
    article: NewsType
}
export const ArticleItem: React.FC<PropsType> = (props) => {
    const { article } = props;
    return (
        <li className="list-group-item" style={{ padding: 30 }}>
            <div className="container">
            <div className="row">
            <div className="col-sm-8" 
        
            style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "flex-start"}}>
           
            <h3 className="mb-1">{article.title}</h3>
            <div style={{ margin: "20px 0" }}>
            {article.description}
          </div>
         
         
              <a href={article.url}>{article.source.name}</a>
           
            <small>{article.publishedAt.split("T")[0]}</small>
          </div>

          <div className="col-sm-4">
          <img className='imgNews' src={article.urlToImage} />
        </div>

            </div>          
            </div>
        </li>
    )
}