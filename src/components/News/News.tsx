import React, { useState } from 'react';
import { getArticles } from '../../api/newsApi';
import { NewsType } from '../../types/types';
import ArticleList from './component/ArticleList';
import SearchBar from './component/SearchBox';


type PropsType = {
    articles: Array<NewsType>
    searchForTopic: (topic: string) => void
}
const News: React.FC<PropsType> = (props) => {

    const [articles, setArticles] = useState([])
    const [totalResults, setTotalResults] = useState('')
    const [searchTopic, setSearchTopic] = useState('')
    const [loading, setLoadin] = useState(false)
    const [apiError, setApiError] = useState('')
    
    const searchForTopic = async (topic: string) => {
        try {
          setLoadin(true)
          const response = await getArticles(topic);
          setArticles(response.articles)
          setSearchTopic(topic)
          setTotalResults(response.totalResults)
        } catch (error) {
          setApiError("Could not find any articles")
        }
        setLoadin(false)
        }
    return (
        <div className='container'>
            <h3 style={{ textAlign: "center", margin: 20 }}>Search for a topic</h3>
            <SearchBar searchForTopic={searchForTopic}/>
            {loading && (
          <p style={{ textAlign: "center" }}>Searching for articles...</p>
        )}
        {articles.length > 0 && (
          <h4 style={{ textAlign: "center", margin: 20 }}>
            Found {totalResults} articles on "{searchTopic}"
          </h4>
        )}
        {articles.length > 0 && <ArticleList articles={articles} />}
        {apiError && <p>Could not fetch any articles. Please try again.</p>}
        </div>
    )
}

export default News;