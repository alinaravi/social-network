import React from 'react';
import moment from "moment";
import './Footer.scss'
export const Footer: React.FC = (props) => {
    return (
        <div>
            <hr />
            <footer className='container-fluid text-center'>
                <div className="row ">
                    <div className="col-6 col-md-4">Copyright &copy; SNet {moment().format("YYYY")} </div>
                    <div className="col-6 col-md-4"><a href="#">Privacy Policy</a></div>
                    <div className="col-6 col-md-4">
                        Created by Alina Ravilova
                        </div>
                </div>
            </footer>
        </div>
    )
}