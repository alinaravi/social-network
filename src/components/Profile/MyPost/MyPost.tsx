import React from 'react';
import classes from './MyPost.module.css'
import Post from './Post/Post';
//props kommen immer, das ist ein Objekt
import AddPostForm, {PostFormValuesType} from './AddPostForm/AddPostForm';
import { PostType } from '../../../types/types';
import './MyPost.scss'

export type MapPropsType = {
  postsData: Array<PostType>
}

export type DispatchPropsType = {
  addPost: (newPostText: string) => void
}

type PropsType = DispatchPropsType & MapPropsType

const MyPost:  React.FC<PropsType> = props => {
  console.log('Render');
  let postsElement =
    [...props.postsData]
      .reverse()
      .map(post => <Post key={post.id} message={post.message} likesCount={post.likesCount} />);



  let addMyPost = (values: PostFormValuesType) => {
    props.addPost(values.postBody);
  };

  return <div className="wrapper">
    <h3 className="title">My posts</h3>
    <div>
      <AddPostForm onSubmit={addMyPost} />
    </div>
    <div>
    {postsElement}
  </div></div>
};

const MyPostMemorized = React.memo(MyPost)
export default MyPostMemorized;