import React from 'react';
import MyPost, { MapPropsType, DispatchPropsType } from './MyPost'
import { actions } from '../../../redux/profileReducer';
import { connect } from 'react-redux';
import { AppStateType } from '../../../redux/reduxStore';
import { PostType } from '../../../types/types';
//props kommen immer, das ist ein Objekt

//Änderung in state, wird diese Function mit einem neuen Obj
//neu Obj nicht gleich alten Obj
const mapStateToProps = (state: AppStateType) => {
  return {
    postsData: state.profilePage.postsData,
  } as MapPropsType
 }



const MyPostContainer = connect<MapPropsType, DispatchPropsType, {}, AppStateType>(mapStateToProps, {
  addPost: actions.addPostActionCreator
}) (MyPost);
export default MyPostContainer;