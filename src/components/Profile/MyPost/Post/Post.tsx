import React, { useState } from 'react';
import classes from './Post.module.css'
import { AiTwotoneLike } from "react-icons/ai";
import '../MyPost.scss'
type PropsType = {
  message: string
  likesCount: any
}
const Post: React.FC<PropsType> = (props) => {

const [count, setCount] = useState(0)

    return (
      <div className='post'>
        <div className='post-cell img-column'>
        <img src='https://i1.pngguru.com/preview/137/834/449/cartoon-cartoon-character-avatar-drawing-film-ecommerce-facial-expression-png-clipart.jpg'/> 
        </div>
        <div className='message-text'>{props.message}</div>
        <div className='post-cell like'>
          <span ><span className='icon' onClick={() => setCount(count + 1)}><AiTwotoneLike /></span>{count}</span>
        </div>

      </div>
    
    ) 
}

export default Post;