import React from 'react'
import { Field, InjectedFormProps, reduxForm } from "redux-form"
import { requiredField } from '../../../../utils/validator/validators'
import { createField, GetStringKeys, Textarea } from '../../../common/FormsControl/FormControls'
import { FaRegPlusSquare } from "react-icons/fa";

type PropsType = {}
type PostFormValuesTypeKeys = GetStringKeys<PostFormValuesType>
export type PostFormValuesType = {postBody: string}

const AddPostForm: React.FC<InjectedFormProps<PostFormValuesType, PropsType> & PropsType> = (props) => {
    return (
        <form onSubmit={props.handleSubmit}>
            <div>
            {createField<PostFormValuesTypeKeys>('Enter your post', 'postBody', [requiredField], Textarea)}
                {/* <Field component={'textarea'} name='postText' /> */}
            </div>
           
            <div className='buttonPost'>
                <button className="btn btn-outline-primary"><FaRegPlusSquare /></button>
            </div>
 <hr />
        </form>
    )
}

export default reduxForm<PostFormValuesType, PropsType>({form: 'profileAddPost'})(AddPostForm)