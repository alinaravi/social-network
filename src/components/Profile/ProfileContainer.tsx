import React, { useEffect, useRef } from 'react';
import Profile from './Profile';
import { connect, useDispatch, useSelector } from 'react-redux';
import { profileThunkCreator, getStatusThunk, updateStatusThunk, savePhoto, saveProfile } from '../../redux/profileReducer'
import { RouteComponentProps, useHistory, useParams, withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { AppStateType } from '../../redux/reduxStore';
import { ProfileType } from '../../types/types';
import Background from '../../asssets/images/18907.jpg'
//Hook

// type PropsType = {
//   saveProfile: (profile: ProfileType) => Promise<any>
//   isOwner: boolean
// }
// type ParamType = {
//   userId: number | null
// }
// export const ProfileContainer: React.FC<PropsType> = (props) => {

// const authUserId = useSelector((state: AppStateType) =>  state.auth.userId)
// const isAuth = useSelector((state: AppStateType) => state.auth.isAuth)

// const dispatch = useDispatch()
// const updateStatusThunk = (status: string) => {dispatch(updateStatusThunk(status))}
// const profileThunkCreator = (userId: number) =>  {dispatch(profileThunkCreator(userId))}
// const getStatusThunk = (userId: number) =>  {dispatch(getStatusThunk(userId))}
// const savePhoto = (file: File) => {dispatch(savePhoto(file))}
// const history = useHistory()
//  const isFirstRender = useRef(true);

//  const refreshProfile = () => {
//   //  let { userId } = useParams<ParamType>();
//    console.log(props)
//   let userId: number | null = +props.match.params.userId;
//     if (!userId) {
//       userId = authUserId;
//       if (!userId) {
//         //todo: may be replace push with Redirect?
//         history.push('/login')
//       }
//     }
//     if(!userId){
//       console.error('ID should exists')
//     } else {
//     profileThunkCreator(userId )
//     getStatusThunk(userId)
//     }
//   }
//   useEffect(() => {
//     refreshProfile()
//   })

//  return <div>
//       <Profile 
//       isOwner={!props.match.params.userId}
//         profile={profile}
//         status={status}
//         updateStatus={updateStatusThunk}
//         savePhoto={savePhoto}
//         saveProfile={props.saveProfile} />
//     </div>
// }
 

//class
type MapStatePropsType = ReturnType<typeof mapStateToProps>
type MapDispatchPropsType = {
  profileThunkCreator: (userId: number) => void
  getStatusThunk: (userId: number) => void
  updateStatusThunk: (status: string) => void 
  savePhoto: (file: File) => void
  saveProfile: (profile: ProfileType) => Promise<any>
}
type PathParamsType = {
  userId: string
}

type PropsType = MapStatePropsType & MapDispatchPropsType & RouteComponentProps<PathParamsType>

class ProfileContainer extends React.Component<PropsType>  {

  refreshProfile() {
    let userId: number | null = +this.props.match.params.userId;
    
   
    if (!userId) {
      userId = this.props.authUserId;
      if (!userId) {
        //todo: may be replace push with Redirect?
        this.props.history.push('/login')
      }
    }
    if(!userId){
      console.error('ID should exists')
    } else {
    this.props.profileThunkCreator(userId )
    this.props.getStatusThunk(userId)
    }
  console.log(this.props.match.params)
  }

  componentDidMount() {
    this.refreshProfile()
  }

  componentDidUpdate(prevProps: PropsType, prevStatus: PropsType,) {
    if(this.props.match.params.userId != prevProps.match.params.userId){
      console.log(prevProps.match.params.userId)
      this.refreshProfile()
    }
  }

  componentWillUnmount(): void {
  }

  render() {
    return (
      <div className='background'>
      <Profile {...this.props}
      isOwner={!this.props.match.params.userId}
        // profile={this.props.profile}
        // status={this.props.status}
        updateStatus={this.props.updateStatusThunk}
        savePhoto={this.props.savePhoto}
        saveProfile={this.props.saveProfile} />
        </div>
    )
  }

}

let mapStateToProps = (state: AppStateType)=> ({
  // profile: state.profilePage.profile,
  // status: state.profilePage.status,
  authUserId: state.auth.userId,
  isAuth: state.auth.isAuth
});

export default compose<React.ComponentType>(
  connect(mapStateToProps, { profileThunkCreator, getStatusThunk, updateStatusThunk, savePhoto, saveProfile }),
  withRouter,
)(ProfileContainer)