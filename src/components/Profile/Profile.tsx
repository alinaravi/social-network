import React, { useEffect } from 'react';
import classes from './Profile.module.css'
import MyPost from './MyPost/MyPost';
import ProfileInfo from './ProfileInfo/ProfileInfo';
import MyPostContainer from './MyPost/MyPostContainer';
import { ProfileType } from '../../types/types';
import { useSelector } from 'react-redux';
import { AppStateType } from '../../redux/reduxStore';
import './ProfileResponse.scss'
import { profileSelector, statusSelector } from '../../redux/profileSelectors';

type PropsType = {
  isOwner: boolean
  // profile: ProfileType
  // status: string
  updateStatus: (status: string) => void
  savePhoto: (file: File) => void
  saveProfile: (profile: ProfileType) => Promise<any>
}

const Profile: React.FC<PropsType> = (props) => {

const profile = useSelector(profileSelector)
const status = useSelector(statusSelector)
useEffect(() => {
  document.title = "Profile | SNet"
}, []);
    return (
      <div className='container'>  
      <div className="row">
      <div className="col-sm-6">
        <ProfileInfo isOwner={props.isOwner} profile={profile} 
        status={status} updateStatus={props.updateStatus}
        savePhoto={props.savePhoto}
        saveProfile={props.saveProfile}
        
         /></div>
         <div className="col-sm-6">
        <MyPostContainer 
      /></div>
      </div>
      </div> 
    ) 
}

export default Profile;