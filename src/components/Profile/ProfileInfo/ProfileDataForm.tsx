import React from 'react';
import { createField, Input, Textarea, GetStringKeys } from '../../common/FormsControl/FormControls';
import { reduxForm, Field, InjectedFormProps } from 'redux-form';
import classes from './ProfileInfo.module.css'
import style from '../../common/FormsControl/FormControls.module.css'
import { requiredField } from '../../../utils/validator/validators';

import { ProfileType } from '../../../types/types';
import '../ProfileForm.scss'

type PropsType = {
  profile: ProfileType
}
type ProfileTypeKeys = GetStringKeys<ProfileType>

const ProfilDataForm: React.FC<InjectedFormProps<ProfileType, PropsType> & PropsType> = (props) => {
  const { handleSubmit, pristine, reset, submitting } = props
  return <div className="wrapper">
  <form onSubmit={props.handleSubmit}>
    
    {props.error && <div className={style.formSummyryError}>
      {props.error}
    </div>
    }
    <div >Your name: {createField<ProfileTypeKeys>("Full name", "fullName", [], Input)}</div>
    <div>Info:
    {createField<ProfileTypeKeys>("About me", "aboutMe", [], Textarea)}</div>
    <div>Looking for a Job:{createField("", "lookingForAJob", [], Input, { type: "checkbox" })}</div>

    <div>My professional skills: {createField<ProfileTypeKeys>("My professional skills", "lookingForAJobDescription", [], Textarea)}</div>

    <div>Contacts: {Object.keys(props.profile.contacts).map(key => {
      return <div key={key} >
        <b>{key}:
      {/* <Field component={Input} placeholder={key} name={"contacts." + key} 
                validate={[]} /> */}


          {/* todo: create some solution for embedded objects  */}
          {createField(key, "contacts." + key, [], Input)}
        </b>
      </div>
     
    })} </div> 
    <div className='buttonPost'><button type="submit" disabled={submitting} className="btn btn-outline-primary">Save</button></div>
  </form>
  </div>
}

const ProfilDataFormReduxForm = reduxForm<ProfileType, PropsType>({ form: 'editProfile' })(ProfilDataForm)


export default ProfilDataFormReduxForm
