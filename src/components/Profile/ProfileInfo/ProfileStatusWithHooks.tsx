import React, { useState, useEffect, ChangeEvent } from 'react';
// import classes from './ProfileInfo.module.css'


type PropsType = {
    isOwner: boolean
    status: string
    updateStatus: (status: string) => void 
  }

const ProfileStatusWithHooks: React.FC<PropsType> = (props) => {

    let [editMode, setEditMode] = useState(false);
    let [status, setStatus] = useState(props.status);

    useEffect(() => {
        setStatus(props.status);
        //abhändigkeit in eckigen klammern
    }, [props.status]);

    const activateEditMode = () => {
        setEditMode(true)
    }

    const deactiveteEditMode = () => {
        setEditMode(false)
        props.updateStatus(status)
    }

    const onStatusChange = (e: ChangeEvent<HTMLInputElement>) => {
        setStatus (e.currentTarget.value )
    }
  

        return (
            <>
                <div>
                    { !editMode &&
                        <div>
                            <span onDoubleClick={activateEditMode}>{props.status || 'No Status'}</span>
                        </div>
                    }{props.isOwner && <div>
                    {editMode &&
                        <div>
                            <input onChange={onStatusChange}
                            onBlur={deactiveteEditMode} autoFocus={true}   
                            value={status}></input>
                            <span className="tooltiptext">Press twice to change status</span>
                        </div>

                    }
                    </div>}
                </div>
            </>
        )
    }

export default ProfileStatusWithHooks;