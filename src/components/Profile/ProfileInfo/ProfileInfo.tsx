import React, { ChangeEvent, useState } from 'react';
import classes from './ProfileInfo.module.css'
import Preloader from '../../common/Preloader/Preloader';
import avatar from '../../../asssets/images/cartoon.jpg' 
import ProfileStatusWithHooks from './ProfileStatusWithHooks';
import ProfilDataForm from './ProfileDataForm';
import { ContactsType, ProfileType } from '../../../types/types';
import { FaPencilAlt } from 'react-icons/fa';

type PropsType = {
  profile: ProfileType | null
  status: string
  isOwner: boolean
  savePhoto: (file: File) => void
  saveProfile: (profile: ProfileType) => Promise<any>
  updateStatus: (status: string) => void
}

const ProfileInfo: React.FC<PropsType> = (props) => {

  let [editMode, setEditMode] = useState(false)

  // const inputFile = useRef(null) 
  if (!props.profile) {
    return <Preloader />
  }

  const onMainPhotoSelected = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.target.files.length) {
      props.savePhoto(e.target.files[0])
    }
  }


  const onSubmit = (formData: ProfileType) => {
    //todo: remove then
    props.saveProfile(formData).then(
      () => {
        setEditMode(false)
      }
    )
  }

  return <div className="wrapper">
    <div className="image-wrapper">

      <img src={props.profile.photos.large ? props.profile.photos.large : avatar} />

      {props.isOwner && <div className='image-upload'><label htmlFor="file-input"><FaPencilAlt /></label><input type={"file"} onChange={onMainPhotoSelected} id='file-input' />
      </div>}

      <div className="description">
        <ProfileStatusWithHooks 
          isOwner = {props.isOwner}
          status={props.status}
          updateStatus={props.updateStatus} />
        
      </div>
      {/* <div>{}Contacts</div> */}
      <hr />
      {editMode
        ? <ProfilDataForm initialValues={props.profile} profile={props.profile} onSubmit={onSubmit} />
        : <ProfilData goToEditMode={() => { setEditMode(true) }} profile={props.profile} isOwner={props.isOwner} />}
      <div></div></div>
  </div>
}

type PropsProfilType = {
  profile: ProfileType
  isOwner: boolean
  goToEditMode: () => void
}

const ProfilData: React.FC<PropsProfilType> = (props) => {
  return <div>
    <div className="name">{props.profile.fullName}</div>
    <div >

      <div><b>Info</b><div className="aboutMe">{props.profile.aboutMe ? props.profile.aboutMe : '---'}</div></div>
  
      <div ><b>My ID:</b> {props.profile.userId}</div>
      <div ><b>Open to work:</b> {props.profile.lookingForAJob ? 'Yes' : 'No'}</div>
      {props.profile.lookingForAJob &&
        <div className="description"><b>My professional skills:</b> {props.profile.lookingForAJobDescription}</div>
      }
      {props.profile.contacts ? <div><b>My Social networking:</b> {Object
        .keys(props.profile.contacts)
        .map((key) => {
          return <Contact key={key} contactTitel={key} contactValue={props.profile.contacts[key as keyof ContactsType]} />
        })} </div> : 'No Contact'}


      {props.isOwner && <div><button onClick={props.goToEditMode} className="btn btn-outline-primary"><FaPencilAlt /></button></div>}
    </div>
  </div>
}

type ContactType = {
  contactTitel: string
  contactValue: string
}

const Contact: React.FC<ContactType> = ({ contactTitel, contactValue }) => {
  return <div>{contactValue ? <div><b>{contactTitel}</b>-<a href={contactValue} target="_blank">{contactValue}</a> </div> : ''}</div>

}

export default ProfileInfo;