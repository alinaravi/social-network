import React, { ChangeEvent } from 'react';
import classes from './ProfileInfo.module.css'

type PropsType = {
    status: string 
    updateStatus: (newStatus: string) => void
}
type StateType = {
    status: string 
    editMode: boolean 
}
class ProfileStatus extends React.Component<PropsType, StateType> {
   
    state = {
        editMode: false,
        status: this.props.status
    }

    activateEditMode = () => {
        console.log('This:', this)
        this.setState({
            editMode: true
        })
    
        
    }

    deactiveteEditMode = () => {
        this.setState({
            editMode: false
        })
        this.props.updateStatus(this.state.status)
    }

    onStatusChange = (e: ChangeEvent<HTMLInputElement>) => {
        this.setState({
            status: e.currentTarget.value 
       })
    }

    componentDidUpdate (prevProps: PropsType, prevState: StateType) {
        //wenn der prevProps nicht gleich neuen, erst dann änder, sonst nicht
        if (prevProps.status!== this.props.status){
        this.setState({
            status: this.props.status
        })}
        
    }


    render() {
        console.log('render')

        return (
            <>
                <div>
                    {!this.state.editMode &&
                        <div>
                            <span onDoubleClick={this.activateEditMode}>{this.props.status || 'No Status'}</span>
                        </div>
                    }
                    {this.state.editMode &&
                        <div>
                            <input onChange={this.onStatusChange} autoFocus={true} onBlur={this.deactiveteEditMode}  
                            value={this.state.status}></input>
                        </div>

                    }
                </div>
            </>
        )
    }
}
export default ProfileStatus;