import React from 'react';
import classes from './Login.module.css'
import { Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {loginThunkCreator} from '../../redux/authReducer'
import { AppStateType } from '../../redux/reduxStore';
import {LoginFormValuesType, LoginReduxForm} from './LoginForm'


export const LoginPage: React.FC = () => {

    const captchaUrl = useSelector((state: AppStateType) => state.auth.captchaUrl)
    const isAuth = useSelector((state: AppStateType) => state.auth.isAuth)

    const dispatch = useDispatch()
    
    const onSubmit = (formData: LoginFormValuesType) => {
        dispatch(loginThunkCreator(formData.email, formData.password, formData.rememberMe, formData.captcha))
    }


    if (isAuth) {
        return <Redirect to={`/profile/`} />
    } 
    return (<div>
       <LoginReduxForm onSubmit={onSubmit} 
                        captchaUrl={captchaUrl} />

    </div>)
}
