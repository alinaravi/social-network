import { reduxForm, Field, InjectedFormProps } from "redux-form";
import {
  createField,
  GetStringKeys,
  Input,
  Textarea,
} from "../common/FormsControl/FormControls";
import { requiredField } from "../../utils/validator/validators";
import React from "react";

import style from "../common/FormsControl/FormControls.module.css";
import "./LoginForm.scss";
import Image from "../../asssets/images/1744.jpg";

const LoginForm: React.FC<
  InjectedFormProps<LoginFormValuesType, LoginFormOwnProps> & LoginFormOwnProps
> = (props) => {
  return (
    //handleSubmit kommt von reduxForm
    <div className="container ">
      <div className="row">
        <div className="col-6">
          <img className="image" src={Image} />
        </div>

        <div className="col-6 loginForm">
          <form onSubmit={props.handleSubmit}>
            <h2>Please Login</h2>
            <div>
              Email:{" "}
              {createField<LoginFormValuesTypeKeys>(
                "Enter your email",
                "email",
                [requiredField],
                Input
              )}
              {/* <Field component={Input} name={'email'} placeholder={'Email'} 
                validate={[requiredField]} /> */}
            </div>
            <div>
              Password:{" "}
              {createField<LoginFormValuesTypeKeys>(
                "Enter your password",
                "password",
                [requiredField],
                Input,
                { type: "password" }
              )}
              {/* <Field component={Input} name={'password'} placeholder={'Password'} type={'password'}
                validate={[requiredField]}/>  */}
            </div>

            <div className="form-group form-check">
              Remember me
              {createField<LoginFormValuesTypeKeys>(
                undefined,
                "rememberMe",
                [],
                Input,
                { type: "checkbox" }
              )}
              {/* <Field component={Input} name={'rememberMe'} type={'checkbox'} /> */}
            </div>
            {props.captchaUrl && <img src={props.captchaUrl} />}
            {props.captchaUrl &&
              createField<LoginFormValuesTypeKeys>(
                "Symbol for image",
                "captcha",
                [requiredField],
                Input,
                {}
              )}

            {props.error && (
              <div className={style.formSummaryError}>{props.error}</div>
            )}
            <div>
              <button className="btn btn-primary">Login</button>
            </div>
          </form>
        </div>
      </div>{" "}
    </div>
  );
};

export const LoginReduxForm = reduxForm<LoginFormValuesType, LoginFormOwnProps>(
  {
    // a unique name for the form
    form: "login",
  }
)(LoginForm);

export type LoginFormValuesType = {
  email: string;
  password: string;
  rememberMe: boolean;
  captcha: string;
};

type LoginFormValuesTypeKeys = GetStringKeys<LoginFormValuesType>;
type LoginFormOwnProps = {
  captchaUrl: string | null;
};
