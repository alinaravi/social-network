import React from 'react';
import classes from './../Dialogs.module.css'
import '../Dialog.scss'
import moment from "moment";
type PropsType = {
    message: string
}

const Message: React.FC<PropsType> = (props) => {
    return (
        <div className='clearfix'>
        <div className="message-data align-right">
             <span className="message-data-time" >{moment().format("MMM Do YY, h:mm:ss a")}</span> &nbsp; &nbsp;
              <span className="message-data-name" >Olia</span> <i className="fa fa-circle me"></i>
            <div className='message other-message float-right'>{props.message}</div>
        </div>
        </div>
    )
}

export default Message;