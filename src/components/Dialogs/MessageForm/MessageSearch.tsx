import { Form, Formik, Field, ErrorMessage } from 'formik';
import React from 'react'
import { BsSearch } from 'react-icons/bs';
import { useSelector } from 'react-redux';
import { FilterType } from '../../../redux/usersReducer';



export const MessageSearchForm: React.FC = React.memo((props) => {


    return <div >
    <div className="input-group flex-nowrap search">
        <div className="input-group-prepend">
            <span className="input-group-text"><BsSearch /></span>
        </div>
        <input type="text" className="form-control" placeholder="search" />
    </div>
    </div>
})




