import React from 'react';
import { reduxForm, Field, InjectedFormProps } from 'redux-form';
import { maxLengthCreator, requiredField } from '../../../utils/validator/validators';
import { createField, GetStringKeys, Input, Textarea } from '../../common/FormsControl/FormControls';
import { NewMessageFormValuesType } from '../Dialogs';

const maxLength10 = maxLengthCreator(10);
type NewMessageFormValuesTypeKeys = GetStringKeys<NewMessageFormValuesType>
type PropsType = {}
export const MessageForm: React.FC<InjectedFormProps<NewMessageFormValuesType, PropsType> & PropsType> = (props) => {
    return (
        <form onSubmit={props.handleSubmit}>
            <div>
                {/* <Field validate={[requiredField, maxLength10]} name="messageBody" component={Textarea} placeholder='Enter your message' /> */}
                {createField<NewMessageFormValuesTypeKeys>('Enter your message', 'newMessageText', [requiredField, maxLength10], Textarea)}
                <i className="fa fa-file-o"></i> &nbsp;&nbsp;&nbsp;
                <i className="fa fa-file-image-o"></i>
                
            </div>
            <div>
                <button className='btn btn-primary'>Send</button>
            </div>
        </form>
    )
}

export const MessageReduxForm = reduxForm<NewMessageFormValuesType>({
    // a unique name for the form
    form: 'dialogMessage'
})(MessageForm)

