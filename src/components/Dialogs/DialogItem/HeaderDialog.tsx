import React from 'react';
import { InitialStateType } from '../../../redux/dialogReducer';
type PropsType = {
    count: number
}

export const Header: React.FC<PropsType> = (props) => {

    return (
        <div className="chat-header clearfix">
            <img src='https://i1.pngguru.com/preview/137/834/449/cartoon-cartoon-character-avatar-drawing-film-ecommerce-facial-expression-png-clipart.jpg' alt="avatar" />
            <div className="chat-about">
                <div className="chat-with">Chat with name of user</div>
                <div className="chat-num-messages">You have already {props.count} messages</div>
            </div>
            <i className="fa fa-star"></i>
        </div>
    )

}



    