import React from 'react';
import classes from './../Dialogs.module.css'
import { NavLink } from 'react-router-dom';
import '../Dialog.scss'

type PropsType = {
    id: number
    name: string
}
const DialogItem: React.FC<PropsType> = (props) => {
    return (
        <li  className='clearfix'>
            <img src='https://i1.pngguru.com/preview/137/834/449/cartoon-cartoon-character-avatar-drawing-film-ecommerce-facial-expression-png-clipart.jpg'/> 
            <div className='about'>
                <NavLink to={`/dialogs/${props.id}`} className='name'>{props.name}</NavLink>
                <div className="status">
              <i className="fa fa-circle online"></i> online
            </div>
                </div>
        </li >
    )

}



export default DialogItem;