import React, { useEffect, useState } from 'react';
import classes from './Dialogs.module.css'
import DialogItem from './DialogItem/DialogItem';
import Message from './Message/Message';
//import { addMessageActionCreator, updateNewMessageTextActionCreator } from '../../redux/dialogReducer';

import { Redirect } from 'react-router-dom';
import { reduxForm, Field, InjectedFormProps } from 'redux-form';
import { requiredField, maxLengthCreator } from '../../utils/validator/validators';
import { createField, Textarea } from '../common/FormsControl/FormControls';
import { InitialStateType } from '../../redux/dialogReducer';
import { MessageReduxForm } from './MessageForm/MessageForm';
import './Dialog.scss'
import { Header } from './DialogItem/HeaderDialog';
import { MessageSearchForm } from './MessageForm/MessageSearch';
import { useDispatch, useSelector } from 'react-redux';
import { AppStateType } from '../../redux/reduxStore';
import { actions, } from '../../redux/dialogReducer';


type PropsType = {
    // dialogPage: InitialStateType
    // sendMessage: (messageText: string) => void
}

export type NewMessageFormValuesType = {
    newMessageText: string
}


const Dialogs: React.FC<PropsType> = (props) => {
  const dialogPage = useSelector((state: AppStateType) => state.dialogPage)
  const dispatch = useDispatch()
  const sendMessage = (messageText: string) => {
    dispatch(actions.sendMessage(messageText))
}
useEffect(() => {
    document.title = "Message | SNet"
 }, []);
 
    let state = dialogPage;
    let countOfUser = Object.keys(state.dialogsData).length
    let countOfMessages = Object.keys(state.messagesData).length

    let dialogsElements = state.dialogsData
        .map((dialog: any) =>
            <DialogItem name={dialog.name} id={dialog.id} key={dialog.id} />)

    let messagesElement = state.messagesData
        .map((mess: any) =>
            <Message message={mess.message} key={mess.id} />)


    // let newMessageElement = state.newMessageText


    let addNewMessage = (values: NewMessageFormValuesType) => {
       sendMessage(values.newMessageText);
    }


    return (
        <div className='background'>
        <div className='container conMess clearfix'>
            <div className='people-list'>
                <MessageSearchForm />
                <div className="chat-num-users"> {countOfUser} users online</div>
                <ul className='list'>{dialogsElements}</ul>
                
            </div>
            <div className='chat'>
                <Header count={countOfMessages}/>
                <div className='chat-history'>{messagesElement}</div>
                <div className='chat-message clearfix'><MessageReduxForm
                    onSubmit={addNewMessage}
                /></div>
            </div>
        </div></div>
    )
}


export default Dialogs;