export type PostType = {
    id: number
    message: string
    likesCount: number
  }

  export type ContactsType = {
    github: string
    vk: string
    facebook: string
    instagram: string
    twitter: string
    website: string
    youtube: string
    mainLink: string
  }
  export type PhotosType = {
    small: string | null
    large: string| null
  }
  export type ProfileType = {
    userId: number
    lookingForAJob: boolean
    lookingForAJobDescription: boolean
    fullName: string
    contacts: ContactsType
    photos: PhotosType
    aboutMe?: string 
  }

  export type UsersDataType = {
    name: string
    id: number
    photos: PhotosType
    status: string
    followed: boolean
}
type SourceType = {
  id: string
  name: string
}
export type NewsType = {
  source: SourceType
  author: string
  title: string
  description: string
  url: string
  urlToImage: string
  publishedAt: string
  content: string
}