import React, { Component, Suspense } from 'react'; 
import './App.css';
import { Route, withRouter, Redirect, Switch } from 'react-router-dom';
// import Header from './components/Header/Header';

import store from './redux/reduxStore'
import Music from './components/Music/Music';
import Counter from './components/News/News';

import Preloader from './components/common/Preloader/Preloader';
import Settings from './components/Setting/Settings';

import {initializeApp} from './redux/appReducer'
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withSuspense } from './hoc/withSuspense';
import { AppStateType } from './redux/reduxStore';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { UsersPage } from './components/Users/UsersContainer';
import { LoginPage } from './components/Login/Login';
import { Footer } from './components/Footer/Footer';
import moment from "moment";
import NavbarComponent from './components/Nav/Nav';
import Dialogs from './components/Dialogs/Dialogs';
import { Friends } from './components/Friends/Friends';
import { NotFound } from './components/common/404NotFound';


const ProfileContainer = React.lazy(() => import('./components/Profile/ProfileContainer'));

type MapPropsType = ReturnType<typeof mapStateToProps>
type DispatchPropsType = {
  initializeApp: () => void
}

const SuspendedDialogs = withSuspense(Dialogs)
const SuspendedProfile = withSuspense(ProfileContainer)

class App extends Component<MapPropsType & DispatchPropsType> {

  catchAllUnhandledErrors = (e: PromiseRejectionEvent) => {
    console.log('Some error');

  }

  componentDidMount() {
    this.props.initializeApp();
    window.addEventListener('unhandledrejection', this.catchAllUnhandledErrors);
    }

    componentWillUnmount(){
      window.removeEventListener('unhandledrejection', this.catchAllUnhandledErrors)
    }

  render() {

  //   if(!this.props.initialized){
  //   return <Preloader />
  // }
  return (
    
      <div >
      {/* <Header /> */}
      {/* <NavbarComponent /> */}
      <Route component={NavbarComponent} />
        <div>
        <Switch>
      <Route exact path='/'  render={()=> <Redirect to={'/profile'} />} />
      <Route path='/dialogs' 
        render={ () => <SuspendedDialogs/>} />
        
      <Route path='/profile/:userId?' 
        render={() => <SuspendedProfile />} />
      
        
      <Route path='/users' render={() => <UsersPage pageTitle='Developers' />} />

      <Route path='/news' component={Counter} />
      <Route path='/music' component={Music} />
      <Route path='/settings' component={Settings} />
      <Route path='/friends' component={Friends} />

      <Route path='/login' render={()=> <LoginPage  />} />
      <Route path='*' render={()=> <NotFound /> } />
     </Switch>
      </div>
      <Footer />
    </div>
    );
  }
}



const mapStateToProps = (state: AppStateType) => ({
  initialized: state.app.initialized
})

let AppContainer = compose<React.ComponentType>(
  withRouter,
  connect(mapStateToProps, {initializeApp}))(App);

const SamuraiJSApp: React.FC = () => {
    return <BrowserRouter>
        <Provider store={store}>
            <AppContainer/>
        </Provider>
    </BrowserRouter>
}

export default SamuraiJSApp;
